//Caio Oliveira
//Diego Pacheco 150123345

#include <stdio.h>
#include <ctype.h>
#define TAM 10

struct ficha{
    int vetor[TAM];
    int i;

};
struct ficha numeros;

//O menu de opções a ser mostrado
void menu(){
    printf("---------------------------------------------------\n");
    printf("0\t\t\tSair\n");
    printf("I\t\tInlcuir o nº\n");
    printf("E\t\tExcluir o nº\n");
    printf("A\t\tAlterar o nº\n");
    printf("O\t\tOrdenar os nº\n");
    printf("P\t\tImprimir os nº\n");
    printf("---------------------------------------------------\n");

}

//Irá inserir os numeros(porém não conseguimos colocar no inicio do numeros, so nos restante
void push(int *numero, int n){
    char opcao;
    int novo;
    printf("---------------------------------------------------\n");
    if(n==0){
        printf("Digite o numero\n");
        scanf("%d",&numero[n]);

    }
    else {
        printf("Escolhas a opcao\n");
        printf("I\t\tInicio\n");
        printf("M\t\tMeio\n");
        printf("F\t\tFim\n");
        scanf("%s",&opcao);
        opcao = toupper(opcao);
        if(opcao == 'M'){
            printf("Digite o numero: \n");
            n = n/2;
            scanf("%d",&numero[n]);

        }
        else if(opcao == 'F'){
            printf("Digite o numero:\n");
            scanf("%d",&numero[n]);

        }

    }
    printf("---------------------------------------------------\n");

}

//Opções de alterações
void pop(int *numero, int n){
    char opcao;
    int i,j,aux,chave;
    printf("---------------------------------------------------\n");
    if(n == 0){
        return;

    }
    else{
        printf("Escolhas a opcao\n");
        printf("I\t\tInicio\n");
        printf("M\t\tMeio\n");
        printf("F\t\tFim\n");
        printf("E\t\tEscolha\n");
        scanf(" %c",&opcao);
        opcao = toupper(opcao);
        if(opcao == 'I'){
            i=0;
            for (j = i; i < n; j++) {
                aux = numero[j+1];
                numero[j] = numero[j+1];
                numero[j+1] = aux;
            }

        }
        else if(opcao == 'M'){
            i= n/2;
            for (j = i; i < n; j++) {
                aux = numero[j+1];
                numero[j] = numero[j+1];
                numero[j+1] = aux;
            }


        }
        else if(opcao == 'F'){
            return;

        }
        else if(opcao == 'E'){
            printf("Digite a chave:\n");
            scanf("%d",&chave);
            for (i = 0; i < n; i++) {
                if(chave == numero[i]){
                    for (j = 0; j < n; j++) {
                        aux = numero[j+2];
                        numero[j] = numero[j+1];
                        numero[j+1] = aux;
                    }

                }
            }

        }
    printf("---------------------------------------------------\n");
    }

}

//Opções de alterações
void alteracao(int *numero, int n){
    int i,novo,chave;
    printf("---------------------------------------------------\n");
    printf("Digite a chave:\n");
    scanf("%d",&chave);
    printf("Digite o novo nº\n");
    scanf("%d",&novo);
    for (i = 0; i < n; i++) {
        numero[i]=novo;
    }
    printf("---------------------------------------------------\n");



}

//Irá imprimir
void imprimir(int *numero, int n){
    int i;
    printf("---------------------------------------------------\n");
    for (i = 0; i < n; i++) {
        printf("%d\n",numero[i]);
    }
    printf("---------------------------------------------------\n");
}

//Irá ordenar os vetores
void ordenar(int *numero, int n){
    int i,j,aux;
    for (i = 0; i < n; i++) {
        for (j = i+j; j < n; j++) {
            if (numero[i] > numero[j]) {
                aux = numero[i];
                numero[i] = numero[j];
                numero[j] = aux;
            }
        }
    }

}

int main ()
{
    char opcao;
    do{
        menu();
        scanf(" %c",&opcao);
        opcao = toupper(opcao);
        switch(opcao){
            case 'I' : push(numeros.vetor,numeros.i);
                       numeros.i++;
                       break;
            case 'E' : pop(numeros.vetor,numeros.i);
                       numeros.i--;
                       break;
            case 'A' : alteracao(numeros.vetor, numeros.i);
                       break;
            case 'P' : imprimir(numeros.vetor, numeros.i);
                       break;
            case 'O' : ordenar(numeros.vetor, numeros.i);
                       break;
            case '0' : break;

        }
    }while(opcao!= '0');


    return 0;
}
